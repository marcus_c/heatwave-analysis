# Installation

Developed and tested in python 3.9.1.

Use the package manager pip to install the required packages.

`pip install -r requirements.txt`

Ensure the `raw_temp_data_london.nc` data file is in the same directory as `analysis.ipynb`.

# Analysis

The `analysis.ipynb` imports the dataset, computes the maximum daily temperature from the hourly data, and calculates the longest annual heatwave per grid-cell per year.

Across all grid-cells and all years, the mean heatwave length is 6.72 days, with a standard deviation of 4.16 days. The maximum heatwave length across the entire dataset is 42 days.

A Mann-Kendall trend test is used to test the presence of an upward (or downward) trend of longest annual heatwave length. This was done for both a randomly selected grid-cell, and the mean longest heatwave length across all grid cells. In both cases, no statistically significant trend was found.

The Mann-Kendall test is non parametric, meaning it is not a distribution specific test, however, any seasonal element of out time series would ideally be removed before performing the test, this has not been done here. Another limitation of this kind of test is a higher likelihood to find no trend if the data has fewer points.

Similarly no trend was found in the ratio between longest annual heatwave lengths in grid-cells (51.5, 0) & (51.75, -1.25). Assuming a constant, time-independent ratio between these two grid-cells, with a mean of 1.14. A heatwave of length 10 days in grid-cell (51.5, 0) would likely mean a heatwave of length 11 days in grid-cell (51.75, -1.25). This approach was tested by predicting the most recent heatwave length in 2019 - 5 days, using only data before 2000. Although this method produced a slight over estimate, the correct value of 5 days was within one standard deviation of the predictions of 7 and 8 days (with and without outlier removal respectively).

A more thorough route for this analysis could be to look at the distribution of longest heatwaves using more than just two points in this area of interest, to see if there is a more distinguishable pattern. For example a nearby point could regularly be the local maximum, with nearby heatwave lengths being characterisable by their distance from this point.

Additionally understanding any potential seasonality would make any time dependence of these variables much more apparent.
